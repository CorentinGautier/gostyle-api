const db = require('../data/indexData');

data = {
  name: 'Test',
  description: 'Test',
  date_start: '2020-01-01',
  date_end: '2020-02-01',
  archived:false,
  promo_pourcentage:50
}

describe("INSERT Qrcode", () => {
  test("NEW Qrcode with data", async (done)=>{
    let res = await db.insert(data);
    expect(typeof res).toBe('object');
    expect(res.length).not.toBe(0);
    expect(res[0].id).not.toBe(undefined || null);
    expect(res[0].qrcode).not.toBe(undefined || null);
    data.id = res[0].id;
    done();
  })
});

describe("GET Qrcode", () => {  
  test("GET all Qrcode", async (done) => {
    let res = await db.getAll();
    expect(typeof res).toBe('object');
    expect(res.length).not.toBe(0);
    res.forEach((qrcode)=>{
      expect(qrcode.id).not.toBe(undefined || null);
      expect(qrcode.qrcode).not.toBe(undefined || null);
    });
    done();
  });
  test("GET one Qrcode", async (done) => {
    let res = await db.getOne(data.id);
    expect(typeof res).toBe('object');
    expect(res.length).toBe(1);
    expect(res[0].id).not.toBe(undefined || null);
    expect(res[0].qrcode).not.toBe(undefined || null);
    done();
  });
});

describe("UPDATE Qrcode", () => {  
  test("MODIFY one Qrcode", async (done) => {
    data.name = 'Test2';
    let res = await db.update(data);
    expect(typeof res).toBe('object');
    expect(res.length).not.toBe(0);
    expect(res[0].name).toBe('Test2');
    done();
  });
});

describe("DELETE Qrcode", () => {  
  test("DELETE one Qrcode", async (done) => {
    let res = await db.delete(data.id);
    expect(typeof res).toBe('object');
    expect(res.length).toBe(1);
    done();
  });
});