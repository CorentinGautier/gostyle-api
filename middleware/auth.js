config = require('../config/config.js');

exports.checkAuth = function(req, res, next){
    //récupère le token
    var token = req.headers['access-token'];

    // decoder le token
    if (token) {
        //vérifier le jeton JWT      
        jwt.verify(token, config.secret, (err, decoded) =>{      
        if (err) {
            return res.json({ message: 'Invalid token' });    
        } else {
            req.decoded = decoded;    
            next();
        }
        });

    } else {
        res.send({ 

            message: 'You need to auth.' 
        });
    }
}