const pg = require("pg");
const config = require('../config/config.js');
const connectionString = "pg://" + config.dbUser + ":" + config.dbPass + "@localhost:5432/gostyle";
const client = new pg.Client(connectionString);
client.connect();

const selectAll = 'SELECT * from "Promotion"';
const selectOne = 'SELECT * from "Promotion" where id=$1';

const insert = `INSERT INTO public."Promotion"
    (name, description, date_start, date_end, archived, date_archived,
    promo_pourcentage, promo_remise, promo_a_partir_de)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *;`;
    
const update = `UPDATE public."Promotion" SET name=$2, description=$3, date_start=$4, date_end=$5,
    archived=$6, date_archived=$7, promo_pourcentage=$8, promo_remise=$9, promo_a_partir_de=$10
    WHERE id=$1 RETURNING *;`;

const del = 'DELETE FROM public."Promotion" WHERE id=$1 RETURNING *;';


module.exports = {

    getAll : async function(){
        let res =  await client.query(selectAll);
        return res.rows;
    },

    getOne : async function(id){
        let res = await client.query(selectOne, [id]);
        return res.rows;
    },

    insert : async function(data){
        let res = await client.query(insert, 
            [
                data.name, data.description, 
                data.date_start, data.date_end, 
                data.archived, data.date_archived,
                data.promo_pourcentage,
                data.promo_remise, data.promo_a_partir_de
            ]);
        return res.rows;
    },

    update : async function(data){
        let res = await client.query(update,
            [
                data.id, data.name, data.description,
                data.date_start, data.date_end,
                data.archived, data.date_archived,
                data.promo_pourcentage, 
                data.promo_remise, data.promo_a_partir_de
            ]);
        return res.rows;
    },

    delete : async function(id){
        let res = await client.query(del, [id]);
        return res.rows;
    }
}