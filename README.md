# gostyle-api

A nodejs microservice to run with gostyle mobile app.

# Statistique du dernier push :

[![coverage report](https://gitlab.com/CorentinGautier/gostyle-api/badges/master/coverage.svg)](https://gitlab.com/CorentinGautier/gostyle-api/commits/master)
[![pipeline status](https://gitlab.com/CorentinGautier/gostyle-api/badges/master/pipeline.svg)](https://gitlab.com/CorentinGautier/gostyle-api/commits/master)

## Set up :

Create "Config/config.js"

```javascript
module.exports = {
  secret: "Your secret key",
  mailUser: "mailUser",
  mailPass: "mailPass",
  dbUser: "dbUser",
  dbPass: "dbPass"
};
```
