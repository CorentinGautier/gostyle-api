const express = require('express');
const services = require('../data/indexData');
const logger = require('../services/log');

const router = express.Router();

/**
 * Get all Qrcode
 * @route GET /qrcode
 * @group GET - GET Qrcode
 * @returns {object} 200 - All QrCode
 * @returns {Error}  default - Unexpected error
 */
router.get('/', async function(req, res){
    let result;
    try{
        result = await services.getAll();
    }catch(err){
        result = err;
        logger.writeLog(err, 'gostyle-api', 'ERROR');
        res.status(400);
    }finally{
        res.send(result);
    }
});

/**
 * Get a single Qrcode
 * @route GET /qrcode/:id
 * @param {uuid} id - id of Qrcode
 * @group GET - GET single Qrcode
 * @returns {object} 200 - Single Qrcode
 * @returns {Error}  default - Unexpected error
 */
router.get('/:id', async function(req, res){
    let result;
    try{
        result = await services.getOne(req.params.id);
        if(result.length == 0){
            res.status(400);
        }
    }catch(err){
        result = err;
        logger.writeLog(err, 'gostyle-api', 'ERROR')
        res.status(400);
    }finally{
        res.send(result);
    }
});


/**
 * Post a new qrcode
 * @route POST /qrcode
 * @group POST - Create qrcode
 * @body name: name of the qrcode, description: description of the qrcode, 
 * date_start: date start of validity qrcode, date_end: date end of validity qrcode, archived :  is this qrcode archived ?, promo_pourcentage: amount of qrcode.
 * @returns {object} 200 - data of qrcode
 * @returns {Error}  default - Unexpected error
 */
router.post('/', async function(req, res){
    let result;
    try{
        let body = req.body;
        result = await services.insert(body);
    }catch(err){
        console.log(err);
        result = err;
        logger.writeLog(err, 'gostyle-api', 'ERROR');
        res.status(400);
    }finally{
        res.send(result);
    }
});


/**
 * Update a qrcode
 * @route PUT /qrcode
 * @group UPDATE - Update qrcode
 * @returns {object} 200 - data of qrcode
 * @returns {Error}  default - Unexpected error
 */
router.put('/', async function(req, res){
    let result;
    try{
        let body = req.body;
        result = await services.update(body);
    }catch(err){
        result = err;
        logger.writeLog(err, 'gostyle-api', 'ERROR');
        res.status(400);
    }finally{
        res.send(result);
    }
});


/**
 * Delete a QrCode
 * @route DELETE /qrcode
 * @group DELETE - Delete QrCode
 * @returns {object} 200 - data about qrcode
 * @returns {Error}  default - Unexpected error
 */
router.delete('/', async function(req, res){
    let result;
    try{
        let body = req.body;
        result = await services.delete(body.id);
    }catch(err){
        result = err;
        logger.writeLog(err, 'gostyle-api', 'ERROR');
        res.status(400);
    }finally{
        res.send(result);
    }
});


module.exports = router;