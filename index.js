//module
express = require('express')
bodyParser = require('body-parser');
morgan = require('morgan');
jwt = require('jsonwebtoken');
config = require('./config/config.js');
mw = require('./middleware/auth');

//services
const logger = require('./services/log.js');

const app = express()
//Headers nécessaire pour autoriser le CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, access-token");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    next();
});

//routes
indexRouter = require('./routes/indexRouter');
authRouter = require('./routes/authRouter');
routeAuth = express.Router();

const expressSwagger = require('express-swagger-generator')(app);

//port de l'api : 3000
app.listen(3000,'0.0.0.0', () => {
    console.log('API Server is now running...');
    logger.writeLog('Server running');
});

app.disable('etag');

//moniteur interne (route: /status)
app.use(require('express-status-monitor')());

//body parser
app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({extended: true}));
app.use(morgan(':date[iso] :method :url :status :res[content-length] - :response-time ms'));

//Pour utiliser le middleware qui vérifie l'auth sur les requêtes /qrcode :
routeAuth.use((req, res, next) =>{
   mw.checkAuth(req, res, next);
});

//routes
app.use('/qrcode', routeAuth, indexRouter);
app.use('/auth', authRouter);

//swagger 
let options = {
    swaggerDefinition: {
        info: { description: 'Documentation pour le microservice gostyle-api', title: 'Swagger', version: '1.0.0', },
        host: 'localhost:3000',
        basePath: '/index',
        produces: [
            "application/json",
        ],
        schemes: ['http', 'https'],
    },
    basedir: __dirname, //app absolute path
    files: ['./routes/*.js'] //Path to the API handle folder
};
expressSwagger(options)
module.exports = app;