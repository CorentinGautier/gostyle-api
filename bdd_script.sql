CREATE DATABASE gostyle
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'French_France.1252'
    LC_CTYPE = 'French_France.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

CREATE SCHEMA public
    AUTHORIZATION postgres;

COMMENT ON SCHEMA public
    IS 'standard public schema';

GRANT ALL ON SCHEMA public TO PUBLIC;

GRANT ALL ON SCHEMA public TO postgres;

CREATE TABLE public."Promotion"
(
    id uuid NOT NULL DEFAULT uuid_in((md5(((random())::text || (clock_timestamp())::text)))::cstring),
    name character varying COLLATE pg_catalog."default" NOT NULL,
    description text COLLATE pg_catalog."default" NOT NULL,
    date_start timestamp with time zone NOT NULL,
    date_end timestamp with time zone NOT NULL,
    archived boolean NOT NULL,
    date_archived timestamp with time zone,
    promo_pourcentage integer,
    promo_remise integer,
    promo_a_partir_de integer,
    CONSTRAINT "Promotion_pkey" PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public."Promotion"
    OWNER to postgres;