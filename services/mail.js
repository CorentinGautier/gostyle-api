var nodemailer = require('nodemailer');
var config = require('../config/config.js');

var mail = {
        send: function (subject, error) {
            var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: config.mailUser,
                    pass: config.mailPass
                }
            });

            var mailOptions = {
                from: config.mailUser,
                to: config.mailUser,
                subject: subject,
                text: subject+error
            };

            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });
    }
};

module.exports = mail;